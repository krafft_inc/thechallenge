'use strict';

var gulp = require('gulp');
var variables = require('./_variables');
var common = require('./_common');
var $ = require('gulp-load-plugins')();
$.wiredep = require('wiredep').stream;

gulp.task('serve', ['jshint', 'serve-watch'], function () {
    var baseDir = [
        variables.src.tmp,
        variables.src.app
    ];
    var filesToSync = [
        variables.src.tmp + variables.filter.html,
        variables.src.tmp + variables.filter.css,
        variables.src.assets + variables.filter.all
        //variables.src.bowerComponents + 'mh-services/dist/' + variables.filter.all
    ];
    filesToSync = filesToSync.concat(variables.src.appJS);
    common.browserSyncInit(baseDir, filesToSync);
});
gulp.task('serve-watch', ['serve-fonts', 'serve-partials'/*, 'serve-styles'*/], serveWatch);

var injectAssetsIntoIndexHtml = function (stream) {
    var jsToInject =
        gulp
            .src(variables.src.appJS)
            .pipe($.naturalSort('asc'))
            .pipe($.angularFilesort());
    return stream
        .pipe($.inject(jsToInject, {relative: true, ignorePath: '../src/'})) //inject public's core scripts
        .pipe($.wiredep({ // inject bower_components js+css
            //exclude: ['jeet.gs'] // exclude some component if needed
        }));
};

gulp.task('serve-partials', function () {
    var indexFilter = $.filter('index.html');

    var str = gulp
        .src(variables.src.JADEParents)
        .pipe($.plumber())
        .pipe($.jade({pretty: true}))
        .pipe(indexFilter);

    return injectAssetsIntoIndexHtml(str)
        .pipe(gulp.dest(variables.src.tmp))
        .pipe(indexFilter.restore())
        .pipe($.cached('compiled-html'))
        .pipe(gulp.dest(variables.src.tmp + variables.src.components));
});

gulp.task('serve-fonts', function () {
    return gulp.src([
        //variables.src.app + variables.src.fonts + '*',
        //variables.src.app + 'bower_components/fontawesome/fonts/*',
        //variables.src.app + 'bower_components/bootstrap/assets/fonts/**/*'
    ])
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe(gulp.dest(variables.src.tmp + variables.src.components + '_global/' + variables.src.fonts));
});

gulp.task('serve-common', ['common-svgs'], function () {

    /* SVG SECTION */
    function fileContents (filePath, file) {
        return file.contents.toString('utf8');
    }

    /* SVG SECTION END */

    return gulp.src(variables.src.app + 'index.html')
        .pipe($.inject(gulp.src(variables.src.tmpSVGSprite), {transform: fileContents}))
        .pipe(gulp.dest(variables.src.tmp));
});


function serveWatch () {
    gulp.watch(variables.src.JADEAll, ['serve-partials']);
    gulp.watch(variables.src.allJS, ['jshint']);
    gulp.watch('bower.json', ['serve-partials']);
}
