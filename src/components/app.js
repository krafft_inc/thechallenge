(function () {
    'use strict';

    angular.module('app', [
        'ui.router',
        'restangular',
        'log.ex.uo',
        'toastr',
        // app modules
        'app.global',
        'app.challenge',
        'app.directives'
    ]);
})();
