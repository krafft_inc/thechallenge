(function(){
    'use strict';

    angular
        .module('app.directives')
        .directive('field', fieldDirective);

    function fieldDirective () {
      return {
          restrict:'E',
          template:'<div class="field"><row ng-repeat="row in rows" cells="cells" row="$index"></row>' +
                        '<button ng-show="rows && cells" class="btn btn-default" ng-click="calculate()">Calculate rectangle</button>' +
                        '<button ng-show="rows && cells" class="btn btn-default" ng-click="reset()">Reset rectangle</button>' +
                        '<button ng-show="rows && cells" class="btn btn-default" ng-click="clean()">Clean field</button>' +
                        '<h3 ng-show="topPositions.length">Chose your options :</h3>' +
                        '<div class="options" ng-class="{active: activeOption == $index}" ng-repeat="options in topPositions" ng-click="activate($index)">{{$index+1}}.Option : {{options}}</div>' +
                    '</div>',
          scope:{
            rows:'=rows',
            cells:'=cells',
              rectangleX:'=rectangleX',  
              rectangleY:'=rectangleY'  
          },
          controller:'FieldCtrl',
          replace:true
      };
    }
    angular
        .module('app.directives')
        .controller('FieldCtrl', FieldCtrl);

    FieldCtrl.$inject = ['$scope'];

    function FieldCtrl($scope){

        var self = this;
        this.activeCells = [];
        this.field = {};

        this.hitsCounter = 0;

        this.hits = {};

        /*
            Field directive protected methods
         */

        /**
         * toggleCell, toggleCells, activates and diactivets clicked cells
         * @param cell
         */
        this.toggleCell = function(cell){
            if(self.activeCells.indexOf(cell) !==-1){
                self.activeCells.splice(self.activeCells.indexOf(cell),1);
                this.field[cell] =false;
            }else{
                self.activeCells.push(cell);
                this.field[cell] =true;
            }
        };

        /**
         * addCell, register cells in field
         * @param cell
         */
        this.addCell = function(cell){
            this.field[cell] =false;
        };

        /*
            Field directive private methods
         */

        /**
         * _calculateRectangle, calculates rectangle square
         *
         * @param startPoint
         * @returns {Array}
         * @private
         */
        function _calculateRectangle(startPoint){

            var rectangle =[];

            for(var i = 0;$scope.rectangleX-1 >=i;i++){

                for(var j = 0; $scope.rectangleY-1 >=j;j++){

                    var point = _parseStartPoint(startPoint);
                    if(point.x+$scope.rectangleX<= $scope.rows.length && point.y+$scope.rectangleY<= $scope.cells.length){
                        rectangle.push((point.x+i)+':'+(point.y+j));
                    }

                }
            }
            return rectangle;
        }

        /**
         * _parseStartPoint, parses directive name to correct start point
         * @param startPoint
         * @returns {{x: Number, y: Number}}
         * @private
         */
        function _parseStartPoint(startPoint){
            var point = startPoint.split(':');
            return {x:parseFloat(point[0]), y:parseFloat(point[1])};
        }

        /**
         * _calculateHits, calculates hits for each start point
         * @private
         */
        function _calculateHits(){
            _.each(_.keys(self.field), function(key){

               var rectangle =  _calculateRectangle(key);
                var hits = _.intersection(rectangle, self.activeCells).length;
                self.hits[key]= hits;
                self.hitsCounter = Math.max(self.hitsCounter, hits);
            });

        }

        /*
            Public API
         */

        /**
         * reset, removes rectangle from field
         */
        $scope.reset = function(){
            $scope.$broadcast('reset');
        };

        /**
         * clean, clean field
         */
        $scope.clean = function(){
            $scope.topPositions = [];
            $scope.$broadcast('clean');
        };
        /**
         * highlight, highlights choosen rectangle
         *
         * @param rect
         */
        $scope.highlight = function(rect){
            _.each(rect, function(cell){
                $scope.$broadcast(cell);
            });
        };
        /**
         * activate, ctivates choosen rectangle
         *
         * @param index
         */
        $scope.activate = function(index){
            $scope.reset();
            $scope.activeOption = index;
            var startPoint = $scope.topPositions[index];
            $scope.highlight(_calculateRectangle(startPoint));
        };

        /**
         * calculate, calculate possible combinations
         */
        $scope.calculate = function(){
            _calculateHits();
            $scope.topPositions = [];
            _.map(self.hits, function(hits, key){

                if(hits === self.hitsCounter){
                    $scope.topPositions.push(key);
                }
            });
            $scope.activate(0);
        };
    }
})();