(function(){
    'use strict';

    angular
        .module('app.directives')
        .directive('row', rowDirective);

    rowDirective.$inject = [];

    function rowDirective () {
        return {
            restrict:'E',
            replace:true,
            scope:{
              cells:'=cells',
              row:'=row'
            },
            template:'<div class="row"><cell ng-repeat="cell in cells" row-number="row" cell-number="$index"></cell></div>'
        };
    }

})();