(function(){
    'use strict';

    angular
        .module('app.directives')
        .directive('cell', cellDirective);

    cellDirective.$inject = [];

    function cellDirective () {
        return {
            restrict:'E',
            replace:true,
            scope:{
              rowNumber:'=rowNumber',
              cellNumber:'=cellNumber'
            },
            require:'^field',
            template:'<div class="cell" ng-click="toggleState()"></div>',
            link:function($scope, el, attr, fieldCtrl){

                $scope.name = _getCellName();

                fieldCtrl.addCell($scope.name);


                /**
                 * toggleState, set cross on clicked cell
                 */
                $scope.toggleState = function(){
                    el.empty();
                    el.toggleClass('clicked');
                    fieldCtrl.toggleCell($scope.name);

                    if(el.hasClass('clicked')){
                        el.append('<i class="fa fa-2x fa-times cross"></i>');

                    }
                };

                /**
                 * _getCellName, returns cell name
                 *
                 * @returns {string}
                 * @private
                 */
                function _getCellName(){
                    return $scope.rowNumber+':'+$scope.cellNumber;
                }

                $scope.$on($scope.name, function(){
                    el.addClass('active');
                });

                $scope.$on('reset', function(){
                    el.removeClass('active');
                });

                $scope.$on('clean', function(){
                    el.removeClass('clicked');
                    el.empty();
                });
            }
        };
    }

})();