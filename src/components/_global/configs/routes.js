'use strict';

angular.module('app.global')
    .config(routesConfig);

routesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
function routesConfig ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('', '/field');
    $urlRouterProvider.otherwise('/404');

    $stateProvider
        .state({
            name: 'root',
            url: '',
            abstract: true,
            template: '<ui-view></ui-view>',
            controller: 'RootCtrl',
            resolve: {}
        })
        .state({
            name: 'root.404',
            url: '/404',
            templateUrl: 'components/_global/views/404.html'
        });
}
