(function() {
    'use strict';

    angular
        .module('app.challenge', [])
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];
    function stateConfig($stateProvider) {


        $stateProvider
            .state({
                name: 'root.field',
                url: '/field',
                controller: 'ChallengeCtrl',
                controllerAs: 'vm',
                templateUrl: 'components/challenge/field.template.html',

            })
            .state({
                name: 'root.rectangle',
                url: '/rectangle',
                controller: 'ChallengeCtrl',
                controllerAs: 'vm',
                templateUrl: 'components/challenge/rectangle.template.html',

            })
            .state({
                name: 'root.game',
                url: '/game',
                controller: 'ChallengeCtrl',
                controllerAs: 'vm',
                templateUrl: 'components/challenge/game.template.html'

            });
    }
})();
