(function(){

    'use strict';

    angular
        .module('app.challenge')
        .controller('ChallengeCtrl', ChallengeCtrl);
    ChallengeCtrl.$inject = ['$log', '$state', 'ChallengeModel'];

    function ChallengeCtrl ($log, $state, ChallengeModel) {

        var challengeModel = ChallengeModel.getInstance();

        var vm = this;
        vm.createFiled = createFiled;
        vm.play = play;

        challengeModel.initialize(vm);

        /**
         * createFiled, set parameters fro field into challenge model
         */
        function createFiled(){
            var iterator = 0;

            while(vm.xasix >iterator){
                vm.rows.push(iterator);
                iterator++;
            }

            iterator = 0;
            while(vm.yasix >iterator){
                vm.cells.push(iterator);
                iterator++;
            }

            challengeModel.setField({
                rows:vm.rows,
                cells:vm.cells
            });

            $state.go('root.rectangle');

        }

        /**
         * play, set parameters fro rectangle into challenge model
         */
        function play(){

            challengeModel.setRectangle({
                rectangleX:vm.rectangleX,
                rectangleY:vm.rectangleY
            });

            $state.go('root.game');

        }
        init();

        function init () {
            $log = $log.getInstance('challenge');
        }

    }

})();

