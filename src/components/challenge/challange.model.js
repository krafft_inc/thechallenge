(function(){

    'use strict';

        function Model(){
            var self = this;
            self.rows = [];
            self.cells = [];
            self.rectangleY = null;
            self.rectangleX = null;
        }

        Model.prototype.setField = function(options){
            this.rows = options.rows || [];
            this.cells = options.cells || [];
        };

        Model.prototype.setRectangle = function(options){
            this.rectangleX = options.rectangleX || null;
            this.rectangleY = options.rectangleY || null;
        };


    Model.prototype.initialize = function(content){
            _.extend(content, this);
        };

        var instance = null;

        var ChallengeModel = {

            getInstance:function(){
                if(instance){
                    return instance;
                }else{
                    instance =  new Model();
                    return instance;
                }
            }

        };



    angular
        .module('app.challenge')
        .value('ChallengeModel', ChallengeModel);

})();

